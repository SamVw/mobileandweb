export interface RecoverPasswordDto {
  newPassword: string;
  recoveryToken: string;
}
