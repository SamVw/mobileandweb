export interface Favorite {
  id: number;
  userId: number;
  itemId: number;
}
