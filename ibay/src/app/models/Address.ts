export interface Address {
  id: number;
  province: string;
  city: string;
  street: string;
  nr: string;
}
