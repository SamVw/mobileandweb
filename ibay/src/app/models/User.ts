import {Address} from './Address';

export interface User {
  token: string;
  fname: string;
  name: string;
  email: string;
  phone: string;
  id: number;
  addresses: Address[];
  password: string;
}
