import {User} from './User';

export interface Bid {
  id: number;
  amount: number;
  itemId: number;
  userId: number;
  user: User;
  date: Date;
}
