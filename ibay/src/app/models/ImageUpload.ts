export interface ImageUpload {
  Content: string;
  fileName: string;
}
