export interface Image {
  id: number;
  itemId: number;
  name: string;
  nameOnly: string;
}
