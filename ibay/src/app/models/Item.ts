import {Image} from './Image';
import {User} from './User';
import {Bid} from './Bid';
import {Favorite} from './Favorite';

export interface Item {
  title: string;
  price: number;
  image: Image[];
  id: number;
  description: string;
  user: User;
  userId: number;
  biddingAllowed: boolean;
  bids: Bid[];
  biddingMinimum: number;
  condition: string;
  images: string[];
  sold: boolean;
  favorites: Favorite[];
  dateCreated: Date;
}
