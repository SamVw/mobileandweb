import { ErrorHandler, Injectable} from '@angular/core';
import {AlertService} from '../_services/alert.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor() { }
  handleError(error) {
    // IMPORTANT: Rethrow the error otherwise it gets swallowed
    if (error.status === 0) {
      return;
    }

    throw error;
  }

}
