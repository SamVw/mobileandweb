import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../_services/authentication.service';
import {RoutingStateService} from '../../_services/routing-state.service';
import {User} from '../../models/User';
import {Address} from '../../models/Address';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  submitted: boolean;
  returnUrl: string;

  registerForm = new FormGroup({
    fname: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(5)]),
    street: new FormControl('', [Validators.required]),
    number: new FormControl('', [Validators.required]),
    city: new FormControl('', [Validators.required]),
    province: new FormControl('', [Validators.required])
  });

  provinces: {id: string, name: string }[] = [
    { id: 'Oost-Vlaanderen', name: 'Oost-Vlaanderen' },
    { id: 'West-Vlaanderen', name: 'West-Vlaanderen' },
    { id: 'Antwerpen', name: 'Antwerpen' },
    { id: 'Limburg', name: 'Limburg' },
    { id: 'Vlaams-Brabant', name: 'Vlaams-Brabant' }
  ];

  @ViewChild('submit') submitButton: any;
  filtersLoaded: Promise<boolean>;
  isLoading: boolean;
  selectedProvince: string;

  constructor(private alertService: AlertService, private router: Router,
              private route: ActivatedRoute, private authenticationService: AuthenticationService,
              private routingState: RoutingStateService) {
    this.submitted = false;

    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }

    this.isLoading = false;
  }

  ngOnInit() {
    this.returnUrl = this.routingState.getPreviousUrl() || '/';

    this.filtersLoaded = Promise.resolve(true);
  }

  onSubmit() {
    this.submitButton.disabled = true;
    this.isLoading = true;

    if (this.registerForm.invalid) {
      this.isLoading = false;
      this.submitButton.disabled = false;
      return;
    }

    if (this.registerForm.get('password').value.toString().length < 5) {
      this.isLoading = false;
      this.submitButton.disabled = false;
      return;
    }

    const address: Address = {
      city: this.registerForm.get('city').value,
      province: this.registerForm.get('province').value,
      street: this.registerForm.get('street').value,
      nr: this.registerForm.get('number').value,
      id: 0
    };

    const user: User = {
      fname: this.registerForm.get('fname').value,
      name: this.registerForm.get('name').value,
      phone: this.registerForm.get('phone').value,
      email: this.registerForm.get('email').value,
      password: this.registerForm.get('password').value,
      addresses: [
        address
      ],
      token: null,
      id: 0
    };

    this.authenticationService.register(user)
      .subscribe(res => {
        this.alertService.success('Successfully registered user!', true);
        this.isLoading = false;
        this.submitButton.disabled = false;
        this.router.navigate([this.returnUrl]);
      },
        error1 => {
        this.alertService.error(error1.error.message);
        this.isLoading = false;
        this.submitButton.disabled = false;
        });
  }

  get password() { return this.registerForm.get('password'); }

  get email() { return this.registerForm.get('email'); }

  get fname() { return this.registerForm.get('fname'); }

  get name() { return this.registerForm.get('name'); }

  get phone() { return this.registerForm.get('phone'); }

  get street() { return this.registerForm.get('street'); }

  get number() { return this.registerForm.get('number'); }

  get city() { return this.registerForm.get('city'); }

  getPasswordErrorMessage() {
    return this.password.hasError('minlength') ? 'Enter at least 5 characters' :
        '';
  }

  getEmailErrorMessage() {
    return this.email.hasError('required') ? 'You must enter an email' :
      this.email.hasError('email') ? 'Not a valid email' :
        '';
  }

  getFNameErrorMessage() {
    return this.fname.hasError('required') ? 'You must enter a first name' :
        '';
  }

  getNameErrorMessage() {
    return this.fname.hasError('required') ? 'You must enter a last name' :
        '';
  }

  getPhoneErrorMessage() {
    return this.phone.hasError('required') ? 'You must enter a phone number' :
        '';
  }

  getStreetErrorMessage() {
    return this.street.hasError('required') ? 'You must enter a street' :
        '';
  }

  getNumberErrorMessage() {
    return this.number.hasError('required') ? 'You must enter a street' :
        '';
  }

  getCityErrorMessage() {
    return this.city.hasError('required') ? 'You must enter a street' :
        '';
  }
}
