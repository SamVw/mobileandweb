import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoveryTokenComponent } from './recovery-token.component';

describe('RecoveryTokenComponent', () => {
  let component: RecoveryTokenComponent;
  let fixture: ComponentFixture<RecoveryTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoveryTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveryTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
