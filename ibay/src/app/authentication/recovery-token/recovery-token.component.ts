import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../_services/alert.service';
import {RecoverPasswordDto} from '../../models/RecoverPasswordDto';
import {AuthenticationService} from '../../_services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-recovery-token',
  templateUrl: './recovery-token.component.html',
  styleUrls: ['./recovery-token.component.scss']
})
export class RecoveryTokenComponent implements OnInit, OnDestroy {
  tokenForm: FormGroup = new FormGroup({
    password: new FormControl('', [Validators.required, Validators.minLength(5)]),
    confirmPassword: new FormControl('', [Validators.required])
  });
  filtersLoaded: Promise<boolean>;
  token: string;
  sub: any;
  isLoading = false;

  constructor(private alertService: AlertService, private authService: AuthenticationService, private route: ActivatedRoute,
              private router: Router) { }

  get password() { return this.tokenForm.get('password'); }

  get passwordC() { return this.tokenForm.get('confirmPassword'); }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.token = params.token;
    });

    this.filtersLoaded = Promise.resolve(true);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onSubmit() {
    this.isLoading = true;

    const password: string = this.tokenForm.get('password').value;
    const passwordConfirming: string = this.tokenForm.get('confirmPassword').value;

    if (password !== passwordConfirming) {
      this.isLoading = false;
      this.password.setErrors({notEqual: true});
      this.passwordC.setErrors({notEqual: true});
      return;
    }

    const dto = {} as RecoverPasswordDto;
    dto.newPassword = password;
    dto.recoveryToken = this.token;

    this.authService.resetPassword(dto).subscribe(res => {
        this.alertService.success('Successfully reset password.');
        this.isLoading = false;
        this.router.navigate(['/login']);
    },
      error => {
      this.alertService.error(error.error);
      this.isLoading = false;
      });
  }

  getPasswordErrorMessage() {
    return this.password.hasError('notEqual') ? 'Passwords do not match.' :
      this.password.hasError('minlength') ? 'Password must contain at least 5 characters.' :
      '';
  }

  getPasswordCErrorMessage() {
    return this.password.hasError('notEqual') ? 'Passwords do not match.' :
        '';
  }
}
