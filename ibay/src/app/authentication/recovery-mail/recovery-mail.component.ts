import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../_services/authentication.service';
import {__values} from 'tslib';
import {AlertService} from '../../_services/alert.service';
import {RoutingStateService} from '../../_services/routing-state.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-recovery-mail',
  templateUrl: './recovery-mail.component.html',
  styleUrls: ['./recovery-mail.component.scss']
})
export class RecoveryMailComponent implements OnInit {

  filtersLoaded: Promise<boolean>;
  mailForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });
  isLoading = false;

  constructor(private authService: AuthenticationService, private alertService: AlertService, private routingService: RoutingStateService,
              private router: Router) {
  }

  get email() { return this.mailForm.get('email'); }

  ngOnInit() {

    this.filtersLoaded = Promise.resolve(true);
  }

  onSubmit() {
    this.isLoading = true;
    const email: string = this.mailForm.get('email').value;

    this.authService.sendRecoveryMail(email).subscribe(res => {
      this.alertService.success('Successfully send mail.');
      this.isLoading = false;
    },
      error => {
        this.alertService.error('Could not send mail.');
        this.isLoading = false;
      });
  }

  getEmailErrorMessage() {
    return this.email.hasError('email') ? 'Enter a valid email.' :
      '';
  }

  back() {
    this.router.navigate([this.routingService.getPreviousUrl()]);
  }
}
