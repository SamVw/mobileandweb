import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginCredentials} from '../../models/LoginCredentials';
import {AlertService} from '../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../_services/authentication.service';
import {RoutingStateService} from '../../_services/routing-state.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  submitted: boolean;
  returnUrl: string;

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
    remember: new FormControl(''),
  });

  get email() { return this.loginForm.get('email'); }

  @ViewChild('submit') submitButton: any;
  filtersLoaded: Promise<boolean>;
  isLoading: boolean;
  promiseSetBySomeAction: Promise<any>;

  constructor(private alertService: AlertService, private router: Router,
              private route: ActivatedRoute, private authenticationService: AuthenticationService,
              private routingState: RoutingStateService, private routingService: RoutingStateService) {
    this.submitted = false;

    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }

    this.isLoading = false;
  }

  ngOnInit() {
    this.returnUrl = this.routingState.getPreviousUrl() || '/';

    if (this.returnUrl.search('recover')) {
      this.returnUrl = '/';
    }

    this.filtersLoaded = Promise.resolve(true);
  }

  onSubmit() {
    this.isLoading = true;
    this.submitButton.disabled = true;

    if (this.loginForm.invalid) {
      this.isLoading = false;
      this.submitButton.disabled = false;
      return;
    }

    const credentials: LoginCredentials = {
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value,
      remember: this.loginForm.get('remember').value
    };

    this.authenticationService.login(credentials.email, credentials.password, credentials.remember)
      .subscribe(res => {
          this.alertService.success('Successfully logged in!', true);
          this.isLoading = false;
          this.submitButton.disabled = false;
          this.router.navigate([this.returnUrl]);
        },
        error1 => {
          this.alertService.error('Email or password are unknown.')
          this.isLoading = false;
          this.submitButton.disabled = false;
      });
  }

  getEmailErrorMessage() {
    return this.email.hasError('email') ? 'Enter a valid email.' :
      '';
  }

  back() {
    this.router.navigate(['/']);
  }
}
