import { Component, OnInit } from '@angular/core';
import {ItemService} from '../../_services/item.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../_services/authentication.service';
import {Item} from '../../models/Item';
import {MatBottomSheet, MatDialog} from '@angular/material';
import {ItemActionsSheetComponent} from '../../content/_dialogs/item-actions-sheet/item-actions-sheet.component';
import {AlertService} from '../../_services/alert.service';
import {ItemTitleComponent} from '../../content/_dialogs/_create-item/item-title/item-title.component';
import {ItemConditionComponent} from '../../content/_dialogs/_create-item/item-condition/item-condition.component';
import {ItemDescriptionComponent} from '../../content/_dialogs/_create-item/item-description/item-description.component';
import {ItemPriceComponent} from '../../content/_dialogs/_create-item/item-price/item-price.component';
import {ItemBiddingComponent} from '../../content/_dialogs/_create-item/item-bidding/item-bidding.component';
import {ItemUploadComponent} from '../../content/_dialogs/_create-item/item-upload/item-upload.component';
import {root} from 'rxjs/internal-compatibility';
import {Ng2ImgMaxService} from 'ng2-img-max';
import {FileLikeObject} from 'ng2-file-upload';
import {forEach} from '@angular/router/src/utils/collection';
import {Image} from '../../models/Image';
import {CONFIG} from '../../models/Config';
import {NotificationService} from '../../_services/notification.service';

@Component({
  selector: 'app-personal-overview',
  templateUrl: './personal-overview.component.html',
  styleUrls: ['./personal-overview.component.scss']
})
export class PersonalOverviewComponent implements OnInit {
  filtersLoaded: Promise<boolean>;
  userId: number = parseInt(this.route.snapshot.params.id);
  items: Item[];
  isLoading = false;
  setOverlay = false;
  resourceUrl = CONFIG.resourceUrl;
  private editMode: boolean;

  constructor(private itemService: ItemService, private route: ActivatedRoute, private authenticationService: AuthenticationService,
              private router: Router, public dialog: MatDialog, private bottomSheet: MatBottomSheet, private alertService: AlertService,
              private ng2ImgMax: Ng2ImgMaxService, private authService: AuthenticationService,
              private notificationService: NotificationService) {
    if (!this.authenticationService.currentUserValue || this.authenticationService.currentUserValue.id !== this.userId) {
      this.router.navigate(['/']);
    }

    this.itemService.getAllPersonalItems(this.userId)
      .subscribe(res => {
        this.items = res;

        this.filtersLoaded = Promise.resolve(true);
      },
        error1 => {
        this.router.navigate(['/']);
        });
  }

  ngOnInit() {
    if (this.authService.currentUserValue) {
      this.notificationService.openSubscription(this.authService.currentUserValue.id);
    }
  }

  openBottomSheet(event: Event): void {
    event.preventDefault();

    const domPath = event.composedPath();
    // @ts-ignore
    const id: number = domPath[2].children[3] ? domPath[2].children[3].value : domPath[1].children[3].value;
    const index: number = this.items.findIndex(value1 => value1.id == id);

    const bottomsheet = this.bottomSheet.open(ItemActionsSheetComponent, {
      data: { itemId: id, sold: this.items[index].sold }
    });

    bottomsheet.afterDismissed().subscribe(value => {
      this.handleUserRequest(value, id, index);
    });
  }

  private handleUserRequest(value, id: number, index: number) {
    switch (value) {
      case 'delete':
        this.startLoading();
        this.itemService.deleteItem(id).subscribe(res => {
            this.alertService.success('Succesfully deleted item');
            this.stopLoading();

            if (index > -1) {
              this.items.splice(index, 1);
            }
          },
          error1 => {
            this.alertService.error('Something went wrong.');
            this.stopLoading();
          });
        break;
      case 'sold':
        this.startLoading();
        this.itemService.markAsSold(id).subscribe(res => {
            this.alertService.success('Successfully marked item as sold');
            this.items[index].sold = true;
            this.stopLoading();
          },
          error1 => {
            this.alertService.error('Something went wrong.');
            this.stopLoading();
          });
        break;
      case 'notSold':
        this.startLoading();
        this.itemService.markAsNotSold(id).subscribe(res => {
            this.alertService.success('Successfully made item available again');
            this.items[index].sold = false;
            this.stopLoading();
          },
          error1 => {
            this.alertService.error('Something went wrong.');
            this.stopLoading();
          });
        break;
      case 'edit':
        this.itemService.getItemDetails(id).subscribe(res => {
          this.editMode = true;
          this.startCreatingItem(res);
        });
        break;
      default:
        break;
    }
  }

  startCreatingItem( item = null) {
    if (!item) {
      item = {} as Item;
    }

    const titleD = this.dialog.open(ItemTitleComponent, { disableClose: true, data: {
        title: (item.title ? item.title : '')
      }
    });
    titleD.afterClosed().subscribe(res => {
      if (res === 'cancel') {
        this.stopLoading();
        return;
      }

      item.title = res;
      this.executeStep2(item);
    });
  }

  private executeStep2(item: Item) {
    const titleD = this.dialog.open(ItemConditionComponent, { disableClose: true, data: {
        condition: (item.condition ? item.condition : '')
      }
    });

    titleD.afterClosed().subscribe(res => {
      if (res === 'cancel') {
        this.stopLoading();
        return;
      } else if (res === 'previous') {
        this.startCreatingItem(item);
        return;
      }

      item.condition = res;
      this.executeStep3(item);
    });
  }

  private executeStep3(item: Item) {
    const titleD = this.dialog.open(ItemDescriptionComponent, { disableClose: true, data: {
        description: (item.description ? item.description : '')
      }
    });
    titleD.afterClosed().subscribe(res => {
      if (res === 'cancel') {
        this.stopLoading();
        return;
      } else if (res === 'previous') {
        this.executeStep2(item);
        return;
      }

      if (!res) {
        item.description = '';
      }
      item.description = res;
      this.executeStep4(item);
    });
  }

  private executeStep4(item: Item) {
    const titleD = this.dialog.open(ItemPriceComponent, { disableClose: true, data: {
        price: (item.price ? +item.price : 0)
      }
    });
    titleD.afterClosed().subscribe(res => {
      if (res === 'cancel') {
        this.stopLoading();
        return;
      } else if (res === 'previous') {
        this.executeStep3(item);
        return;
      }

      item.price = res !== '' ?  res : 0;
      this.executeStep5(item);
    });
  }

  private executeStep5(item: Item) {
    const titleD = this.dialog.open(ItemBiddingComponent, { disableClose: true, data: { price: item.price,
        biddingAllowed: (item.biddingAllowed ? +item.biddingAllowed : false),
        biddingMinimum: (item.biddingMinimum ? +item.biddingMinimum : 0) } });
    titleD.afterClosed().subscribe(res => {
      if (res === 'cancel') {
        this.stopLoading();
        return;
      } else if (res === 'previous') {
        this.executeStep4(item);
        return;
      }

      item.biddingAllowed = res.biddingAllowed;
      item.biddingMinimum = item.biddingAllowed ? res.biddingMinimum : 0;
      this.executeStep6(item);
    });
  }

  private executeStep6(item: Item) {
    const titleD = this.dialog.open(ItemUploadComponent, { disableClose: true, data: { item } });
    titleD.afterClosed().subscribe(res => {
      if (res === 'cancel') {
        this.stopLoading();
        return;
      } else if (res === 'previous') {
        this.executeStep5(item);
        return;
      }

      this.stopLoading();

      const files: FileLikeObject[] = res !== null ? res : null;
      this.submit(item, files);
    });
  }

  submit(item: Item, files: FileLikeObject[]) {
    if (files) {
      item.images = [];
      files.forEach(value => {
        item.images.push(value.name.toString().replace(/\s+/g, ''));
      });
    } else {
      item.images = [];
    }

    this.startLoading();
    if (this.editMode) {
      this.itemService.putItem(item, item.id).subscribe(res => {
          this.uploadImages(res, files);
        },
        error => {
          if (error.status === 400) {
            this.alertService.error(error.error);
          } else if (error.status === 500) {
            this.alertService.error('Something went wrong at the server');
          } else {
            this.alertService.error('Check your internet connection');
          }
          this.stopLoading();
          this.startCreatingItem(item);
        });
    } else {
      this.itemService.postItem(item).subscribe(res => {
          this.uploadImages(res, files);
        },
        error => {
          if (error.status === 400) {
            this.alertService.error(error.error);
          } else if (error.status === 500) {
            this.alertService.error('Something went wrong at the server');
          } else {
            this.alertService.error('Check your internet connection');
          }
          this.stopLoading();
          this.startCreatingItem(item);
        });
    }
  }

  private uploadImages(item: Item, images: FileLikeObject[]) {
    let lastUpload = false;
    if (images.length > 0) {
      images.forEach((image, index) => {
        // @ts-ignore
        this.ng2ImgMax.compressImage(image.rawFile, 0.100).subscribe(
          result => {
            // @ts-ignore
            const compressedImage = new File([result], result.name);

            if (index === images.indexOf(images[images.length - 1])) {
              lastUpload = true;
            }

            // @ts-ignore
            this.SendImageToServer(item , item.image[item.image.findIndex(i => i.nameOnly === image.name)], compressedImage, lastUpload);
          },
          error => {
            this.alertService.error('Something went wrong while compressing image...');
            // this.stopLoading();
            // this.startCreatingItem(item);
          }
        );
      });
    } else {
      this.alertService.success('Successfully created item');

      if (this.editMode) {
        this.items.splice(this.items.findIndex(i => i.id == item.id), 1);
      }
      this.items.unshift(item);

      this.stopLoading();
    }
  }

  private SendImageToServer(item: Item, image: Image, file: File, lastUpload: boolean) {
    const formData = new FormData();
    formData.append('file', file, root);
    formData.append('path', image.name.toString());

    this.itemService.postImage(formData).then(res => {
        if (lastUpload) {
          if (this.editMode) {
            this.items.splice(this.items.findIndex(i => i.id == item.id), 1);
          }
          this.items.unshift(item);
          this.editMode = false;

          this.stopLoading();
          this.alertService.success('Successfully created item');
        }
      },
      error1 => {
        this.alertService.error('Image could not be uploaded');
        this.startCreatingItem(item);
    });
  }

  private stopLoading() {
    this.isLoading = false;
    this.setOverlay = false;
  }

  private startLoading() {
    this.isLoading = true;
    this.setOverlay = true;
  }
}
