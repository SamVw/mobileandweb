import { Component, OnInit } from '@angular/core';
import {ItemService} from '../../_services/item.service';
import {Item} from '../../models/Item';
import {AuthenticationService} from '../../_services/authentication.service';
import {AlertService} from '../../_services/alert.service';
import {NotificationService} from '../../_services/notification.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  items: Item[];
  filtersLoaded: Promise<boolean>;
  userId: number = parseInt(this.route.snapshot.params.id);

  constructor(private itemService: ItemService, private authService: AuthenticationService, private alertService: AlertService,
              private notificationService: NotificationService, private route: ActivatedRoute, private router: Router) {
    this.itemService.loadFavorites(this.userId).subscribe(res => {
        this.items = res;
        this.filtersLoaded = Promise.resolve(true);
    },
      error1 => {
        if (error1.status === 401) {
          this.router.navigate(['/']);
          return;
        }

        this.alertService.error('Could not retrieve items');
        this.filtersLoaded = Promise.resolve(true);
      });
  }

  ngOnInit() {
    if (this.authService.currentUserValue) {
      this.notificationService.openSubscription(this.authService.currentUserValue.id);
    }
  }

  removeItem($event: number) {
    this.items.splice( this.items.findIndex(i => i.id === $event), 1);
  }
}
