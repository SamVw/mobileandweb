import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Item} from '../models/Item';
import {Bid} from '../models/Bid';
import {CONFIG} from '../models/Config';
import {timeout} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private baseUrl: string;

  constructor(private http: HttpClient) {
    // this.baseUrl = 'https://localhost:44335/api';
    this.baseUrl = CONFIG.apiUrl;
  }

  public getFirstItems(size: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/items/page-size/${size}`);
  }

  public async getPageOfItems(start: number, end: number): Promise<Item[]> {
    return await this.http.get<Item[]>(`${this.baseUrl}/items/page/${start}/${end}`).toPromise();
  }

  public getAllPersonalItems(id: number): Observable<Item[]> {
    return this.http.get<Item[]>(`${this.baseUrl}/items/user/${id}`);
  }

  public getItemDetails(id: number): Observable<Item> {
    return this.http.get<Item>(`${this.baseUrl}/items/${id}`);
  }

  public getBidsFormItem(id: number): Observable<Bid[]> {
    return this.http.get<Bid[]>(`${this.baseUrl}/bids/item/${id}`);
  }

  public placeBid(bid: Bid): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/bids`, bid);
  }

  public postItem(item: Item): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/items`, item);
  }

  public async postImage(imageDto: FormData): Promise<any> {
    return await this.http.post<any>(`${this.baseUrl}/images/upload`, imageDto).toPromise();
  }

  public deleteItem(itemId: number): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/items/${itemId}`);
  }

  public markAsSold(itemId: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/items/${itemId}/sold/true`);
  }

  public markAsNotSold(itemId: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/items/${itemId}/sold/false`);
  }

  removeImageFromServer(itemId: any, imageId: any): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/items/${itemId}/image/${imageId}`);
  }

  putItem(item: Item, id: number): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/items/${id}`, item);
  }

  loadFavorites(userId: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/items/favorites/${userId}`);
  }
}
