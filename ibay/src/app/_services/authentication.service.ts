import { Injectable } from '@angular/core';
import {BehaviorSubject, config, Observable} from 'rxjs';
import {User} from '../models/User';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {AlertService} from './alert.service';
import {RecoverPasswordDto} from '../models/RecoverPasswordDto';
import {CONFIG} from '../models/Config';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private baseUrl: string;
  configUrl = CONFIG.apiUrl;

  constructor(private http: HttpClient, private alertService: AlertService) {
    this.currentUserSubject = new BehaviorSubject<User>(null);

    if (localStorage.getItem('currentUser')) {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    } else if (sessionStorage.getItem('currentUser')) {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
    }

    this.currentUser = this.currentUserSubject.asObservable();

    //this.baseUrl = 'https://localhost:44335/api';
    this.baseUrl = this.configUrl;
  }

  public get currentUserValue(): User {
    if (this.currentUserSubject.value) {
      return this.currentUserSubject.value;
    }
  }

  login(email: string, password: string, remember: boolean) {
    return this.http.post<any>(`${this.baseUrl}/users/authenticate`, { email, password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          if (remember) {
            localStorage.setItem('currentUser', JSON.stringify(user));
          } else {
            sessionStorage.setItem('currentUser', JSON.stringify(user));
          }

          this.currentUserSubject.next(user);
        }

        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    sessionStorage.removeItem('currentUser');

    this.currentUserSubject.next(null);
  }

  private handleLoginError(error: any) {
    this.alertService.error('Wrong email or password.');
  }

  register(user: User) {
    return this.http.post<User>(`${this.baseUrl}/users/register`, user);
  }

  public sendRecoveryMail(email: string): Observable<any> {
    return this.http.post<User>(`${this.baseUrl}/users/password/recoverymail`, { email });
  }

  resetPassword(dto: RecoverPasswordDto): Observable<any> {
    return this.http.post<User>(`${this.baseUrl}/users/password/recover`, dto);
  }
}
