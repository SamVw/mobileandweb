import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CONFIG} from '../models/Config';
import {BehaviorSubject} from 'rxjs';
import {SwPush} from '@angular/service-worker';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  configUrl = CONFIG.apiUrl;
  public subscription: PushSubscription;
  readonly VAPID_PUBLIC_KEY = 'BKDi3R4V7Px6RXtLFq0p72Fz8MckAI4e11h1wMXBhV5ChATHhRHoxmS3qw2Ftux81iAoY8K1DT_mi94p-ObUAW4';

  constructor(private http: HttpClient, private swPush: SwPush, private router: Router) {
  }

  public openSubscription(userId: number) {
    if (!this.subscription) {
      this.swPush.requestSubscription({
        serverPublicKey: this.VAPID_PUBLIC_KEY
      })
        .then(sub => {
          this.subscription = sub;
          this.http.post(`${this.configUrl}/notifications`, { sub, userId }).subscribe();
        })
        .catch(err => console.error('Could not subscribe to notifications', err));
    }

    this.swPush.notificationClicks.subscribe( notpayload => {
      if (notpayload.action === 'viewItem') {
        this.router.navigate(['item/' + notpayload.notification.data.itemId]);
      }
    });
  }

  public sendNotifications() {
    return this.http.post(`${this.configUrl}/notifications/send`, null);
  }

  public unsubscribe(endpoint: string) {
    return this.http.delete(`${this.configUrl}/notifications/${encodeURIComponent(endpoint)}`);
  }
}
