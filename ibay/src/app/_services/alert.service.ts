import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {NavigationStart, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';


@Injectable()
export class AlertService {
  private subject = new Subject<any>();
  private closing = new Subject<boolean>();
  private keepAfterNavigationChange = false;
  private timeoutOne: any;
  private timeoutTwo: any;

  constructor(private router: Router) {
    // clear alert message on route change
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterNavigationChange) {
          // only keep for a single location change
          this.keepAfterNavigationChange = false;
        } else {
          // clear alert
          this.subject.next();
        }
      }
    });
  }

  success(message: string, keepAfterNavigationChange = false) {
    this.clearTimeouts();

    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.closing.next(false);
    this.subject.next({ type: 'success', text: message });

    this.timeoutOne = setTimeout(() => {
      this.closing.next(true);

      this.timeoutTwo = setTimeout(() => this.subject.next(), 500);
    }, 3000);
  }

  error(message: string, keepAfterNavigationChange = false) {
    this.clearTimeouts();

    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.closing.next(false);
    this.subject.next({ type: 'error', text: message });

    this.timeoutOne = setTimeout(() => {
      this.closing.next(true);

      this.timeoutTwo = setTimeout(() => this.subject.next(), 1500);
      }, 3000);
  }

  private clearTimeouts() {
    if (this.timeoutOne) {
      clearTimeout(this.timeoutOne);
    }
    if (this.timeoutTwo) {
      clearTimeout(this.timeoutTwo);
    }
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  getClosing(): Observable<boolean> {
    return this.closing.asObservable();
  }
}

