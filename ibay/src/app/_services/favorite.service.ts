import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CONFIG} from '../models/Config';
import {Favorite} from '../models/Favorite';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
  private baseUrl: string;

  constructor(private http: HttpClient) {
    // this.baseUrl = 'https://localhost:44335/api';
    this.baseUrl = CONFIG.apiUrl;
  }

  createFavorite(favorite: Favorite): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/favorites`, favorite);
  }

  removeFavorite(id: number) {
    return this.http.delete<any>(`${this.baseUrl}/favorites/${id}`);
  }
}
