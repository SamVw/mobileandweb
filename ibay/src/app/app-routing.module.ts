import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OverviewComponent} from './content/overview/overview.component';
import {LoginComponent} from './authentication/login/login.component';
import {RegisterComponent} from './authentication/register/register.component';
import {DetailComponent} from './content/detail/detail.component';
import {PersonalOverviewComponent} from './management/personal-overview/personal-overview.component';
import {RecoveryMailComponent} from './authentication/recovery-mail/recovery-mail.component';
import {RecoveryTokenComponent} from './authentication/recovery-token/recovery-token.component';
import {FavoritesComponent} from './management/favorites/favorites.component';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
  { path: '', component: OverviewComponent, data: { preload: true } },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'recover/mail', component: RecoveryMailComponent },
  { path: 'recover/:token', component: RecoveryTokenComponent },
  { path: 'item/:id', component: DetailComponent, data: { preload: true } },
  { path: '%23/item/:id', component: DetailComponent, data: { preload: true } },
  { path: 'item/user/:id', component: PersonalOverviewComponent, data: { preload: true }, canActivate: [AuthGuard] },
  { path: 'item/user/:id/favorites', component: FavoritesComponent, data: { preload: true }, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
