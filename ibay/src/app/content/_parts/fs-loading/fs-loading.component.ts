import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-fs-loading',
  templateUrl: './fs-loading.component.html',
  styleUrls: ['./fs-loading.component.scss']
})
export class FsLoadingComponent implements OnInit {

  @Input() message = '';
  @Input() overlay = false;

  constructor() { }

  ngOnInit() {
  }

}
