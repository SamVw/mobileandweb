import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FsLoadingComponent } from './fs-loading.component';

describe('FsLoadingComponent', () => {
  let component: FsLoadingComponent;
  let fixture: ComponentFixture<FsLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FsLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FsLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
