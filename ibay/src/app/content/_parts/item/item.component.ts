import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Item} from '../../../models/Item';
import {AlertService} from '../../../_services/alert.service';
import {CONFIG} from '../../../models/Config';
import {AuthenticationService} from '../../../_services/authentication.service';
import {ItemService} from '../../../_services/item.service';
import {FavoriteService} from '../../../_services/favorite.service';
import {Favorite} from '../../../models/Favorite';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() item: Item;

  @Output() unfavored = new EventEmitter<number>();

  resourceUrl = CONFIG.resourceUrl;
  isFavorite: boolean;

  constructor(private alertService: AlertService, public authService: AuthenticationService, private favoriteService: FavoriteService) {
  }

  ngOnInit() {
    if (this.authService.currentUserValue) {
      this.isFavorite = this.item.favorites.findIndex(f => f.userId === this.authService.currentUserValue.id) !== -1;
    }
  }

  toggleFavorite() {
    if (!this.isFavorite) {
      const favorite = {
        userId: this.authService.currentUserValue.id,
        itemId: this.item.id
      } as Favorite;

      this.favoriteService.createFavorite(favorite).subscribe(res => {
        this.item.favorites.push(res);
        this.isFavorite = true;
      });
    } else {
      const index = this.item.favorites.findIndex(f => f.userId === this.authService.currentUserValue.id);
      const favorite = this.item.favorites[this.item.favorites.findIndex(f => f.userId === this.authService.currentUserValue.id)];
      this.favoriteService.removeFavorite(favorite.id).subscribe(() => {
        this.item.favorites.splice(index, 1);
        this.isFavorite = false;

        this.unfavored.emit(this.item.id);
      });
    }
  }
}
