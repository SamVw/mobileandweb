import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../../../models/Item';
import {element} from 'protractor';
import {IAlbum, Lightbox} from 'ngx-lightbox';
import {Image} from '../../../models/Image';
import {CONFIG} from '../../../models/Config';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  @Input() images: Image[];
  avatars;
  album: IAlbum[];

  title = 'Details of item';

  carouselOptions = {
    margin: 25,
    rewind: true,
    autoplay: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 1,
        nav: true,
        loop: false
      }
    }
  };
  resourceUrl = CONFIG.resourceUrl;

  constructor(private lightbox: Lightbox) {
  }

  ngOnInit(): void {
    this.avatars = [];
    this.images.forEach(img => this.avatars.push({title: '', url: img.name}));
    this.album = [];
    this.images.forEach(img => {
      this.album.push({src: img.name, thumb: '', caption: ''});
    });
  }

}
