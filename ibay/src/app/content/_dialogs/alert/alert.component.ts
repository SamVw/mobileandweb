import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AlertService} from '../../../_services/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {

  private subscriptionMessage: Subscription;
  private subscriptionClosing: Subscription;
  message: any;
  closing: boolean;

  constructor(private alertService: AlertService) {
  }

  ngOnInit() {
    this.subscriptionMessage = this.alertService.getMessage().subscribe(message => {
      this.message = message;
    });

    this.subscriptionClosing = this.alertService.getClosing().subscribe(closing => {
      this.closing = closing;
    });
  }

  ngOnDestroy() {
    this.subscriptionMessage.unsubscribe();
    this.subscriptionClosing.unsubscribe();
  }

  removeAlert() {
    this.closing = true;
    this.message = null;
  }
}
