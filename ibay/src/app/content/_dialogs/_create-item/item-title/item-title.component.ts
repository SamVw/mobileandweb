import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-item-title',
  templateUrl: './item-title.component.html',
  styleUrls: ['./item-title.component.scss']
})
export class ItemTitleComponent implements OnInit {
  form = this.fb.group({
    title: ['', Validators.required]
  })

  constructor(public dialogRef: MatDialogRef<ItemTitleComponent>, private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any) { }

  get title() { return this.form.get('title'); }

  ngOnInit() {
    this.form.get('title').setValue(this.data.title);
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.dialogRef.close(this.form.get('title').value);
  }

  onNoClick(): void {
    this.dialogRef.close('cancel');
  }

  getTitleErrorMessage() {
    return this.title.hasError('required') ? 'You must enter a title' :
      '';
  }
}
