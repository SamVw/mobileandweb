import {Component, ElementRef, Inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FileLikeObject, FileUploader} from 'ng2-file-upload';
import {ItemService} from '../../../../_services/item.service';

@Component({
  selector: 'app-item-upload',
  templateUrl: './item-upload.component.html',
  styleUrls: ['./item-upload.component.scss']
})
export class ItemUploadComponent implements OnInit {
  form = this.fb.group({
    upload: ['']
  });
  public uploader: FileUploader = new FileUploader({});

  constructor(public dialogRef: MatDialogRef<ItemUploadComponent>, private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any,
              private itemService: ItemService) { }

  ngOnInit() {
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const file: File = this.form.get('upload') ? this.form.get('upload').value : null;
    if (file && file.type !== 'image/jpeg' && file.type !== 'image/png') {
      return;
    }

    this.dialogRef.close(this.getFiles());
  }

  onNoClick(): void {
    this.dialogRef.close('cancel');
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('upload').setValue(file);
    } else {
      this.form.get('upload').setValue(null);
    }
  }

  previous() {
    this.dialogRef.close('previous');
  }

  getFiles(): FileLikeObject[] {
    return this.uploader.queue.map((fileItem) => {
      return fileItem.file;
    });
  }

  removeFromServer(id: any, $event: MouseEvent) {
    const target: Element = $event.target as Element;
    this.itemService.removeImageFromServer(this.data.item.id, id).subscribe(res  => {
      target.parentElement.parentElement.remove();
      this.data.item.image.splice(this.data.item.image.findIndex(i => i.id == id), 1);
    });
  }
}
