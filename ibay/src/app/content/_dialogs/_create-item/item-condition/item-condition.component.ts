import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-item-condition',
  templateUrl: './item-condition.component.html',
  styleUrls: ['./item-condition.component.scss']
})
export class ItemConditionComponent implements OnInit {
  condition = new FormControl('', Validators.required);
  form = this.fb.group({
    condition: this.condition
  })

  constructor(public dialogRef: MatDialogRef<ItemConditionComponent>, private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  get condtionF() { return this.form.get('condition'); }

  ngOnInit() {
    this.form.get('condition').setValue(this.data.condition.toString() !== '' ? this.data.condition.toString() : '0');
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.dialogRef.close(this.form.get('condition').value);
  }

  onNoClick(): void {
    this.dialogRef.close('cancel');
  }

  getConditionErrorMessage() {
    return this.condtionF.hasError('required') ? 'You must select a condition' :
      '';
  }

  previous() {
    this.dialogRef.close('previous');
  }
}
