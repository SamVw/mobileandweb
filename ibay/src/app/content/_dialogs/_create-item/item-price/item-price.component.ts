import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-item-price',
  templateUrl: './item-price.component.html',
  styleUrls: ['./item-price.component.scss']
})
export class ItemPriceComponent implements OnInit {
  form = this.fb.group({
    price: ['', Validators.min(0)]
  });

  constructor(public dialogRef: MatDialogRef<ItemPriceComponent>, private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any) { }

  get price() { return this.form.get('price'); }

  ngOnInit() {
    this.form.get('price').setValue(+this.data.price);
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.dialogRef.close(this.form.get('price').value);
  }

  onNoClick(): void {
    this.dialogRef.close('cancel');
  }

  getPriceErrorMessage() {
    return this.price.hasError('required') ? 'You must enter a price' :
      '';
  }

  previous() {
    this.dialogRef.close('previous');
  }
}
