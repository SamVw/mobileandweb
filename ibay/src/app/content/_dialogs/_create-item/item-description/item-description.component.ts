import {Component, Inject, OnInit} from '@angular/core';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-item-description',
  templateUrl: './item-description.component.html',
  styleUrls: ['./item-description.component.scss']
})
export class ItemDescriptionComponent implements OnInit {
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    placeholder: 'Enter text here...',
    maxHeight: '40vh'
  };

  form = this.fb.group({
    description: ['']
  });

  constructor(public dialogRef: MatDialogRef<ItemDescriptionComponent>, private fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.form.get('description').setValue(this.data.description);
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.dialogRef.close(this.form.get('description').value);
  }

  onNoClick(): void {
    this.dialogRef.close('cancel');
  }

  previous() {
    this.dialogRef.close('previous');
  }
}
