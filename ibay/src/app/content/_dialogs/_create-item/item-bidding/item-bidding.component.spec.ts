import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemBiddingComponent } from './item-bidding.component';

describe('ItemBiddingComponent', () => {
  let component: ItemBiddingComponent;
  let fixture: ComponentFixture<ItemBiddingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemBiddingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemBiddingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
