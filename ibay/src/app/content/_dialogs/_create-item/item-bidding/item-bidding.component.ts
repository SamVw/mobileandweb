import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-item-bidding',
  templateUrl: './item-bidding.component.html',
  styleUrls: ['./item-bidding.component.scss']
})
export class ItemBiddingComponent implements OnInit {
  form = this.fb.group({
    biddingMinimum: ['']
  })

  constructor(public dialogRef: MatDialogRef<ItemBiddingComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder) { }

  @ViewChild('biddingAllowed') biddingAllowed: any;

  get bidding() { return this.form.get('biddingMinimum'); }

  ngOnInit() {
    this.form.get('biddingMinimum').setValue(+this.data.biddingMinimum !== 0 ? +this.data.biddingMinimum : this.data.price);
    this.biddingAllowed.checked = this.data.biddingAllowed;
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    if (this.bidding.value > this.data.price) {
      this.bidding.setErrors({tooHigh: true});
      return;
    }

    this.dialogRef.close({ biddingMinimum: this.form.get('biddingMinimum').value, biddingAllowed: this.biddingAllowed.checked });
  }

  onNoClick(): void {
    this.dialogRef.close('cancel');
  }

  toggle() {

  }

  getBiddingErrorMessage() {
    return this.bidding.hasError('tooHigh') ? 'Minimum bid must be lower then set price.' :
      '';
  }

  previous() {
    this.dialogRef.close('previous');
  }
}
