import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Router} from '@angular/router';
import {ItemService} from '../../../_services/item.service';
import {AuthenticationService} from '../../../_services/authentication.service';
import {AlertService} from '../../../_services/alert.service';

@Component({
  selector: 'app-bidding',
  templateUrl: './bidding.component.html',
  styleUrls: ['./bidding.component.scss']
})
export class BiddingComponent {

  @ViewChild('bidInput') bidInput: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<BiddingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router, private itemService: ItemService, private authService: AuthenticationService,
    private alertService: AlertService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  placeBid() {
    const inputValue = this.bidInput.nativeElement.value;
    if (this.data.bids.length === 0
      || this.bidInput.nativeElement.value && (this.data.bids.length > 0 && inputValue > this.data.bids[0].amount)) {
      this.dialogRef.close(this.bidInput.nativeElement.value);
    } else {
      this.alertService.error('Amount must be higher then current highest bid.');
    }
  }
}
