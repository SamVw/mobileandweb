import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-prompt',
  templateUrl: './login-prompt.component.html',
  styleUrls: ['./login-prompt.component.scss']
})
export class LoginPromptComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<LoginPromptComponent>, private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.router.navigate(['/login']);
    this.dialogRef.close();
  }

}
