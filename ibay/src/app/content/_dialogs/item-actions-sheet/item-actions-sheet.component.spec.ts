import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemActionsSheetComponent } from './item-actions-sheet.component';

describe('ItemActionsSheetComponent', () => {
  let component: ItemActionsSheetComponent;
  let fixture: ComponentFixture<ItemActionsSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemActionsSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemActionsSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
