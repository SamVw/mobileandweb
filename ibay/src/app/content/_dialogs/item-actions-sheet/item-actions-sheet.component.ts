import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';
import {ItemService} from '../../../_services/item.service';
import {AlertService} from '../../../_services/alert.service';

@Component({
  selector: 'app-item-actions-sheet',
  templateUrl: './item-actions-sheet.component.html',
  styleUrls: ['./item-actions-sheet.component.scss']
})
export class ItemActionsSheetComponent implements OnInit {

  isLoading = false;

  constructor(private bottomSheetRef: MatBottomSheetRef<ItemActionsSheetComponent>, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
              private itemService: ItemService, private alertService: AlertService) {}

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  ngOnInit() {
  }

  deleteItem() {
    this.bottomSheetRef.dismiss('delete');
  }

  edit() {
    this.bottomSheetRef.dismiss('edit');
  }

  sold($event: MouseEvent) {
    this.bottomSheetRef.dismiss('sold');
  }

  notSold($event: MouseEvent) {
    this.bottomSheetRef.dismiss('notSold');
  }
}
