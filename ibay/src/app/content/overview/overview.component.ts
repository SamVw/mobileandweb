import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Item} from '../../models/Item';
import {ItemService} from '../../_services/item.service';
import {AlertService} from '../../_services/alert.service';
import {PageEvent} from '@angular/material';
import {SwPush} from '@angular/service-worker';
import {NotificationService} from '../../_services/notification.service';
import {AuthenticationService} from '../../_services/authentication.service';
import {Observable} from 'rxjs';
import {CONFIG} from '../../models/Config';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  filtersLoaded: Promise<boolean>;
  opened: boolean;
  loadedItems = [];
  visibleItems: Item[];
  conditions: [{ viewValue: string; value: string }, { viewValue: string; value: string }] = [
    {value: '1', viewValue: 'Used'},
    {value: '0', viewValue: 'New'}
  ];
  provinces: {id: string, name: string }[] = [
    { id: 'Oost-Vlaanderen', name: 'Oost-Vlaanderen' },
    { id: 'West-Vlaanderen', name: 'West-Vlaanderen' },
    { id: 'Antwerpen', name: 'Antwerpen' },
    { id: 'Limburg', name: 'Limburg' },
    { id: 'Vlaams-Brabant', name: 'Vlaams-Brabant' }
  ];

  @ViewChild('searchInput') search: ElementRef;
  @ViewChild('sidenav') sidenav: any;
  @ViewChild('minValue') minValue: ElementRef;
  @ViewChild('maxValue') maxValue: ElementRef;
  selectedCondition: string;
  selectedProvince: string;
  itemsFound: number;
  pageLength = 0;
  fullLength = 0;
  pageSize = 10;
  pageIndex = 0;
  isLoading = false;
  overlay = false;
  message = '';

  constructor(private itemService: ItemService, private alertService: AlertService, private swPush: SwPush,
              private notificationService: NotificationService, private authService: AuthenticationService) {
    this.loadAllItems();
  }

  ngOnInit() {
    if (this.authService.currentUserValue) {
      this.notificationService.openSubscription(this.authService.currentUserValue.id);
    }
  }

  private loadAllItems() {
    this.itemService.getFirstItems(this.pageSize).subscribe(res => {
        this.pageLength = res.totalAmountOfItems;
        this.fullLength = this.pageLength;
        this.loadedItems.push(res.items);
        this.visibleItems = this.loadedItems[this.pageIndex];

        this.filtersLoaded = Promise.resolve(true);

        this.minValue.nativeElement.value = 0;
        this.maxValue.nativeElement.value = this.getMaximumPrice();

        this.itemsFound = this.pageLength;

        this.getOtherPages();
      },
      error1 => {
        this.alertService.error('Could not retrieve items');
        this.filtersLoaded = Promise.resolve(true);
      });
  }

  private async getOtherPages() {
    let index = 1;
    for (let i = this.pageSize + 1; i <= this.pageLength; i += this.pageSize) {
      const end = (i + this.pageSize) <= this.pageLength ? i + this.pageSize : this.pageLength;
      await this.itemService.getPageOfItems(i, end).then(res => {
        this.isLoading = false;
        this.overlay = false;
        this.message = '';

        this.loadedItems.push(res);
        if (this.pageIndex === index) {
          this.visibleItems = this.loadedItems[this.pageIndex];
        }
        index ++;
      });
    }
  }

  private getMaximumPrice(): number {
    let max = 0;
    this.loadedItems.forEach(arr => {
      const items: Item[] = arr;
      items.forEach(item => {
        if (item.price > max) {
          max = item.price;
        }
      });
    });

    return max;
  }

  filterOnSearch() {
    this.visibleItems = [] as  Item[];
    this.loadedItems.forEach(value => {
      value.forEach(item => {
        this.visibleItems.push(item);
      });
    });

    if (this.search.nativeElement.value !== '') {
      this.visibleItems = this.visibleItems.filter(obj => {
        return obj.title.toLowerCase().search(this.search.nativeElement.value.toLowerCase()) !== -1;
      });
    }

    if (this.selectedCondition) {
      this.visibleItems = this.visibleItems.filter(obj => {
        // @ts-ignore
        return obj.condition === +this.selectedCondition;
      });
    }

    if (this.maxValue.nativeElement && this.minValue.nativeElement) {
      const min = parseInt(this.minValue.nativeElement.value);
      const max = parseInt(this.maxValue.nativeElement.value);
      if (max > min) {
        this.visibleItems = this.visibleItems.filter(obj => {
          return obj.price >= min && obj.price <= max;
        });
      } else {
        this.alertService.error('Maximum price must be higher than minimum price');
        return;
      }
    }

    if (this.selectedProvince) {
      this.visibleItems = this.visibleItems.filter(obj => {
        // @ts-ignore
        return obj.user.addresses[0].province === this.selectedProvince;
      });
    }

    this.itemsFound = this.visibleItems.length;
    this.pageLength = this.itemsFound;
    this.pageIndex = 0;

    this.sidenav.toggle();
  }

  clear() {
    this.search.nativeElement.value = '';
    this.visibleItems = this.loadedItems[this.pageIndex];
    this.selectedCondition = '';
    this.pageLength = this.fullLength;
    this.itemsFound = this.pageLength;
    this.minValue.nativeElement.value = 0;
    this.maxValue.nativeElement.value = this.getMaximumPrice();
    this.sidenav.toggle();
  }

  pageEvent($event: PageEvent) {
    this.isLoading = false;
    this.overlay = false;
    this.message = '';

    this.pageIndex = $event.pageIndex;
    if (this.loadedItems[this.pageIndex]) {
      this.visibleItems = this.loadedItems[this.pageIndex];
    } else {
      this.isLoading = true;
      this.overlay = true;
      this.message = 'Loading additional items';
    }

    window.scroll(0, 0);
  }
}
