import {Component, OnDestroy, OnInit, Pipe, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Item} from '../../models/Item';
import {ItemService} from '../../_services/item.service';
import {AlertService} from '../../_services/alert.service';
import {ContactComponent} from '../_dialogs/contact/contact.component';
import {MatDialog, MatTable} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {BiddingComponent} from '../_dialogs/bidding/bidding.component';
import {AuthenticationService} from '../../_services/authentication.service';
import {Bid} from '../../models/Bid';
import {LoginPromptComponent} from '../_dialogs/login-prompt/login-prompt.component';
import {RoutingStateService} from '../../_services/routing-state.service';
import {NotificationService} from '../../_services/notification.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  itemLoaded: Promise<boolean>;
  bidsLoaded: Promise<boolean>;

  @ViewChild(MatTable) table: MatTable<any>;

  id: number;
  sub: any;
  item: Item;
  bids: Bid[];
  biddingData = [];
  displayedColumns: string[] = ['amount', 'name', 'date'];
  isLoading = false;
  overlay = false;

  constructor(private route: ActivatedRoute, private itemService: ItemService, private router: Router, private alertService: AlertService,
              public dialog: MatDialog, private authenticationService: AuthenticationService, private routingState: RoutingStateService,
              private authService: AuthenticationService, private notificationService: NotificationService) {
  }

  ngOnInit() {
    if (this.authService.currentUserValue) {
      this.notificationService.openSubscription(this.authService.currentUserValue.id);
    }

    this.sub = this.route.params.subscribe(params => {
      this.id = +params.id; // (+) converts string 'id' to a number

      this.itemService.getItemDetails(this.id).subscribe(res => {
          this.item = res;
          this.itemLoaded = Promise.resolve(true);
        },
        error => {
          this.alertService.error('Item does not exist!');
          this.router.navigate(['/']);
      });

      this.itemService.getBidsFormItem(this.id).subscribe(res => {
        this.bids = res;

        this.biddingData = [];
        this.bids.forEach((bid, i) => {
          const date = new Date(bid.date).getDate() + '/' + new Date(bid.date).getMonth() + '/' + new Date(bid.date).getFullYear();
          this.biddingData.push({date , name: bid.user.fname + ' ' + bid.user.name[0] + '.', amount: bid.amount});
        });

        this.bidsLoaded = Promise.resolve(true);
      });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  viewProfile() {
    this.dialog.open(ContactComponent, {
      data: this.item.user
    });
  }

  openBiddingDialog() {
    if (this.authenticationService.currentUserValue) {
      const dialog = this.dialog.open(BiddingComponent, {
        data: {item: this.item, user: this.authenticationService.currentUserValue, bids: this.bids}
      });

      dialog.afterClosed().subscribe(amount => {
        if (amount) {
          this.handleBid(amount);
        }
      });
    } else {
      this.dialog.open(LoginPromptComponent);
    }
  }

  private handleBid(amount: number) {
    this.isLoading = true;
    this.overlay = true;

    const bid = {} as Bid;
    bid.date = new Date();
    bid.amount = +amount;
    bid.userId = this.authService.currentUserValue.id;
    bid.itemId = this.item.id;
    this.itemService.placeBid(bid).subscribe(res => {
        this.biddingData.forEach((row, i) => {
          if (row.name === res.user.fname + ' ' + res.user.name[0] + '.') {
            this.biddingData.splice(i, 1);
          }
        });

        const date = new Date(bid.date).getDate() + '/' + new Date(bid.date).getMonth() + '/' + new Date(bid.date).getFullYear();
        this.biddingData.unshift({
          date, name: this.authenticationService.currentUserValue.fname + ' '
            + this.authenticationService.currentUserValue.name[0] + '.', amount: bid.amount
        });

        this.table.renderRows();

        this.updateBids(bid);

        this.alertService.success('Your bid has been placed or updated successfully');

        this.isLoading = false;
        this.overlay = false;
      },
      error => {
        if (error.status === 400) {
          this.alertService.error(error.error);
        } else if (error.status === 500) {
          this.alertService.error('Something went wrong at the server');
        } else {
          this.alertService.error('Check your internet connection');
        }
        this.isLoading = false;
        this.overlay = false;
    });
  }

  private updateBids(bid) {
    this.bids.forEach((b, i) => {
      if (b.userId === bid.userId) {
        this.bids.splice(i, 1);
      }
    });
    this.bids.unshift(bid);
  }

  goBack() {
    this.router.navigate([this.routingState.getPreviousUrl()]);
  }
}
