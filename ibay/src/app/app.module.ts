import { BrowserModule } from '@angular/platform-browser';
import {ErrorHandler, LOCALE_ID, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatBottomSheetModule,
  MatButtonModule,
  MatCardModule, MatCheckboxModule, MatChipsModule, MatDialogModule, MatExpansionModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatListModule, MatMenuModule, MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
  MatSidenavModule, MatSlideToggleModule, MatSnackBarModule, MatTableModule,
  MatToolbarModule
} from '@angular/material';
import { OverviewComponent } from './content/overview/overview.component';
import { LoginComponent } from './authentication/login/login.component';
import { RegisterComponent } from './authentication/register/register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AlertService} from './_services/alert.service';
import {JwtInterceptor} from './_helpers/jwt.interceptor';
import { ItemComponent } from './content/_parts/item/item.component';
import { DetailComponent } from './content/detail/detail.component';
import {NgxHmCarouselModule} from 'ngx-hm-carousel';
import { CarouselComponent } from './content/_parts/carousel/carousel.component';
import { LoadingComponent } from './content/_parts/loading/loading.component';
import { ContactComponent } from './content/_dialogs/contact/contact.component';
import {LightboxModule} from 'ngx-lightbox';
import { SafePipe } from './_pipes/safe.pipe';
import { BiddingComponent } from './content/_dialogs/bidding/bidding.component';
import {RoutingStateService} from './_services/routing-state.service';
import { LoginPromptComponent } from './content/_dialogs/login-prompt/login-prompt.component';
import { PersonalOverviewComponent } from './management/personal-overview/personal-overview.component';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {OwlModule} from 'ngx-owl-carousel';
import {Ng2ImgMaxModule} from 'ng2-img-max';
import { ItemActionsSheetComponent } from './content/_dialogs/item-actions-sheet/item-actions-sheet.component';
import { RecoveryMailComponent } from './authentication/recovery-mail/recovery-mail.component';
import { RecoveryTokenComponent } from './authentication/recovery-token/recovery-token.component';
import {AvatarModule} from 'ngx-avatar';
import { AlertComponent } from './content/_dialogs/alert/alert.component';
import {LaddaModule} from 'angular2-ladda';
import { FsLoadingComponent } from './content/_parts/fs-loading/fs-loading.component';
import { ItemTitleComponent } from './content/_dialogs/_create-item/item-title/item-title.component';
import { ItemConditionComponent } from './content/_dialogs/_create-item/item-condition/item-condition.component';
import { ItemDescriptionComponent } from './content/_dialogs/_create-item/item-description/item-description.component';
import { ItemPriceComponent } from './content/_dialogs/_create-item/item-price/item-price.component';
import { ItemBiddingComponent } from './content/_dialogs/_create-item/item-bidding/item-bidding.component';
import { ItemUploadComponent } from './content/_dialogs/_create-item/item-upload/item-upload.component';
import {FileUploadModule} from 'ng2-file-upload';
import {NotificationService} from './_services/notification.service';
import { ItemTitleEditComponent } from './content/_dialogs/_edit-item/item-title-edit/item-title-edit.component';
import {NetworkStatusService} from './_services/network-status.service';
import {GlobalErrorHandler} from './_helpers/global-error-handler.interceptor';
import {FavoriteService} from './_services/favorite.service';
import { FavoritesComponent } from './management/favorites/favorites.component';

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    LoginComponent,
    RegisterComponent,
    ItemComponent,
    DetailComponent,
    CarouselComponent,
    LoadingComponent,
    ContactComponent,
    SafePipe,
    BiddingComponent,
    LoginPromptComponent,
    PersonalOverviewComponent,
    ItemActionsSheetComponent,
    RecoveryMailComponent,
    RecoveryTokenComponent,
    AlertComponent,
    FsLoadingComponent,
    ItemTitleComponent,
    ItemConditionComponent,
    ItemDescriptionComponent,
    ItemPriceComponent,
    ItemBiddingComponent,
    ItemUploadComponent,
    ItemTitleEditComponent,
    FavoritesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    MatSnackBarModule,
    HttpClientModule,
    MatExpansionModule,
    MatMenuModule,
    MatCheckboxModule,
    NgxHmCarouselModule,
    MatDialogModule,
    LightboxModule,
    MatTableModule,
    MatChipsModule,
    MatSelectModule,
    AngularEditorModule,
    MatProgressSpinnerModule,
    OwlModule,
    Ng2ImgMaxModule,
    MatBottomSheetModule,
    AvatarModule,
    LaddaModule.forRoot({
      style: 'expand-left',
      spinnerSize: 40,
      spinnerColor: 'green',
      spinnerLines: 12
    }),
    MatPaginatorModule,
    FileUploadModule
  ],
  providers: [
    AlertService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    RoutingStateService,
    // { provide: LocationStrategy, useClass: HashLocationStrategy },
    NotificationService,
    NetworkStatusService,
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    FavoriteService,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ContactComponent,
    BiddingComponent,
    LoginPromptComponent,
    ItemActionsSheetComponent,
    ItemTitleComponent,
    ItemConditionComponent,
    ItemDescriptionComponent,
    ItemPriceComponent,
    ItemBiddingComponent,
    ItemUploadComponent
  ]
})
export class AppModule { }
