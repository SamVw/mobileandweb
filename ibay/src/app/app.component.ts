import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {MatSnackBar} from '@angular/material';
import {SwPush, SwUpdate} from '@angular/service-worker';
import {AuthenticationService} from './_services/authentication.service';
import {Router} from '@angular/router';
import {User} from './models/User';
import {AlertService} from './_services/alert.service';
import {RoutingStateService} from './_services/routing-state.service';
import {NotificationService} from './_services/notification.service';
import {NetworkStatusService} from './_services/network-status.service';
import {fromEvent, Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'ibay';

  mobileQuery: MediaQueryList;

  @ViewChild('snav') snav: any;
  @ViewChild('arrowSpan') arrowSpan: ElementRef;

  private mobileQueryListener: () => void;
  loggedIn: boolean;
  public user: User;
  private profileDown: boolean;

  onlineEvent: Observable<Event>;
  offlineEvent: Observable<Event>;
  subscriptions: Subscription[] = [];

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private swUpdate: SwUpdate,
              private authenticationService: AuthenticationService, private router: Router, private renderer: Renderer2,
              private alertService: AlertService, private routingState: RoutingStateService, private swPush: SwPush,
              private notificationService: NotificationService, private networkService: NetworkStatusService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);

    this.loggedIn = false;
    this.authenticationService.currentUser.subscribe(value => {
      if ( value !== null) {
        this.loggedIn = true;
        this.user = value;
      }
    });

    this.profileDown = false;

    routingState.loadRouting();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this.mobileQueryListener);

    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }


  ngOnInit(): void {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('New version available. Load New Version?')) {
          window.location.reload();
        }
      });
    }

    this.onlineEvent = fromEvent(window, 'online');
    this.offlineEvent = fromEvent(window, 'offline');

    this.subscriptions.push(this.onlineEvent.subscribe(e => {
      this.alertService.success('Internet connection has been restored!', true);
      window.location.reload();
    }));

    this.subscriptions.push(this.offlineEvent.subscribe(e => {
      this.alertService.error('Internet connection was lost!', true);
    }));
  }

  logout() {
    this.authenticationService.logout();
    this.loggedIn = false;

    if (this.notificationService.subscription) {
      this.swPush.unsubscribe();
    }

    this.alertService.success('Successfully logged out!');
    this.closeMenu('/login');
  }

  switchArrow() {
    this.profileDown = !this.profileDown;
    this.renderer.addClass(this.arrowSpan.nativeElement, 'rotate');
  }

  closeMenu(location: string) {
    this.snav.close();

    this.router.navigate([location]);
  }
}
